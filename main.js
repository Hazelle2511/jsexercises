//Challenge1
let name = prompt(`What's  your name?`)
document.getElementById('text').innerHTML = `Bonjour ${name}`

//Challenge 2
document.getElementById("loginbtn").addEventListener("click", function(){

  let username=document.getElementById("username").value;
let password=document.getElementById("password").value;

  if(username === "lea@gmail.com" && password === "abE353_5")

    {
      alert("Login Successful");
      return false;
    }

    else
    { 
      alert("Login Failed"); 
    }
})
// Challenge 4

let val1 = Math.floor(Math.random() * 10);
let val2 = Math.floor(Math.random() * 10);

let typeOperations = ["+", "-", "*"];
let operation =
  typeOperations[Math.floor(Math.random() * typeOperations.length)];
let question = "Quel est le résultat de " + val1 + operation + val2;

document.querySelector("#question").innerHTML = question;

let answer;

if (operation == "+") {
  answer = val1 + val2;
} else if (operation == "-") {
  answer = val1 - val2;
} else {
  answer = val1 * val2;
}

document.querySelector(".btn").addEventListener("click", function () {
  let userInput = document.getElementById("response").value;
  if (userInput == answer) {
    alert("Good job");
  } else {
    alert("Try again");
  }
});

// console.log(`the value is ${userInput}`);
// console.log(typeOperations);
// console.log(typeOperations{index});
console.log(question);

// Challenge 5

let totalNotes = 0;
let validateNotes = document.getElementById("btn");

validateNotes.addEventListener("click", finalNotes);
function finalNotes(x) {
  let noteOne = parseFloat(document.getElementById("input-number1").value);
  let noteTwo = parseFloat(document.getElementById("input-number2").value);
  let note3 = parseFloat(document.getElementById("input-number3").value);
  let note4 = parseFloat(document.getElementById("input-number4").value);
  let note5 = parseFloat(document.getElementById("input-number5").value);
  let note6 = parseFloat(document.getElementById("input-number6").value);
  let note7 = parseFloat(document.getElementById("input-number7").value);
  let note8 = parseFloat(document.getElementById("input-number8").value);
  let note9 = parseFloat(document.getElementById("input-number9").value);
  let note10 = parseFloat(document.getElementById("input-number10").value);

  sum = parseFloat(
    noteOne +
      noteTwo +
      note3 +
      note4 +
      note5 +
      note6 +
      note7 +
      note8 +
      note9 +
      note10
  );
let totalNotes = sum / 10;
  // console.log(sum);
  // console.log(note2);
  console.log = function () {
    document.querySelector("#current-notes").innerHTML = totalNotes;
  };
  console.log(totalNotes);

  // let totalNotes = finalNotes / 10;
}

// //Challenge 5
// var i = 0;
// var somme = 0;
// var moyenne = 0;
// var nb_notes = 10;
// nb_notes = prompt(
//   "Souhaitez-vous faire la moyenne de combien de notes ?",
//   "Nb de Notes"
// );
// notes = new Array(nb_notes);
// for (i = 1; i <= nb_notes; i++) {
//   note_i = prompt("Entrez la note");
//   notes[i - 1] = parseInt(note_i);
//   somme += notes[i - 1];
// }
// moyenne = somme / nb_notes;
// alert("La moyenne des notes est : " + moyenne);

//Challenge 6
//  Game values
let min = 1,
  max = 10,
  winningNum = getRandomNum(min, max),
  guessesLeft = 3;

//UI Elements
const game = document.querySelector("#game"),
  minNum = document.querySelector(".min-num"),
  maxNum = document.querySelector(".max-num"),
  guessBtn = document.querySelector("#guess-btn"),
  guessInput = document.querySelector("#guess-input"),
  message = document.querySelector(".message");

// Assign UI min and max
minNum.textContent = min;
maxNum.textContent = max;

guessBtn.addEventListener("click", function () {
  let guess = parseInt(guessInput.value);

  // Validate
  if (isNaN(guess) || guess < min || guess > max) {
    setMessage(`Please enter a number between ${min} and ${max}`, `red`);
  }

  // Check if won
  if (guess === winningNum) {
    // Game Over -- won

    gameOver(true, `${winningNum} is correct! YOU WIN!`);
  } else if (guess >= winningNum) {
    alert("Lesser than your guess");
  } else {
    alert("Larger than your guess");
  }
});

// Game over
function gameOver(won, msg) {
  let color;
  won === true ? (color = "green") : (color = "red");
  // Disable input
  guessInput.disabled = true;
  // Change border color
  guessInput.style.borderColor = color;
  // Set text color
  message.style.color = color;
  // Set message
  setMessage(msg);

  //Play Again
  guessBtn.value = "Play Again";
  guessBtn.className += "play again";
}

// Get Winning Number
function getRandomNum(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

// Set message
function setMessage(msg, color) {
  message.style.color = color;
  message.textContent = msg;
}
